package com.dtdevel.classroom.library.dataaccess;

import java.util.List;

import com.dtdevel.classroom.library.models.LibraryBook;
import com.mongodb.DBObject;

public class LibraryBookDataAccess extends ModelDataAccess<LibraryBook> {
	
	private final static String COLLECTION = "LibraryBooks";

	private static LibraryBookDataAccess instance;
	public static LibraryBookDataAccess getInstance() {
		ModelDataAccess.COLLECTION = COLLECTION;
		if (instance == null)
			instance = new LibraryBookDataAccess();
		return instance;
	}

	@Override
	public List<LibraryBook> find(DBObject query, DBObject sort) {
		return find(query, sort, LibraryBook.class);
	}
}
