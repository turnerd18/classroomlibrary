package com.dtdevel.classroom.library.dataaccess;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.util.JSON;

public abstract class ModelDataAccess<M> {

	protected static String COLLECTION; 
	
	private MongoClient client;
	private static Gson gson = new Gson();
	
	private final static String DB_URI = "mongodb://classroom-library:wln94HGIebJP9pCHrXsi@ds053429.mongolab.com:53429/heroku_app28021001";
		
	/**
	 * Connect and get mongo collection
	 * @param collectionName
	 * @return
	 * @throws UnknownHostException
	 */
	protected DBCollection getCollection() throws UnknownHostException {
		MongoClientURI uri  = new MongoClientURI(DB_URI); 
        client = new MongoClient(uri);
        DB db = client.getDB(uri.getDatabase());
        DBCollection collection = db.getCollection(COLLECTION);
		return collection;
	}
	
	public void dropCollection() {
		try {
			DBCollection collection = getCollection();
			collection.drop();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Close mongo database connection
	 */
	protected void closeConnection() {
		if (client != null)
			client.close();
	}
		
	/**
	 * 
	 * @param query
	 * @param sort
	 * @param clazz
	 * @param collectionName
	 * @return
	 */
	protected List<M> find(DBObject query, DBObject sort, Class<M> clazz) {
		List<M> models = new ArrayList<M>();
		
		try {
			DBCollection collection = getCollection();
			DBCursor docs;
			if (query == null && sort == null) {
				docs = collection.find();
			} else if (sort == null) {
				docs = collection.find(query);
			} else {
				docs = collection.find(query).sort(sort);
			}
			while (docs.hasNext()) {
				models.add(convertJson(docs.next(), clazz));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection();
		}
		
		return models;
	}
	
	/**
	 * Insert one instance of model
	 * @param model
	 */
	public void insert(M model) {
		try {
			DBCollection collection = getCollection();
			DBObject dbObject = convertModel(model);
			collection.save(dbObject);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection();
		}
	}
	
	/**
	 * Insert many instances of model
	 * @param models
	 */
	public void insert(List<M> models) {
		try {
			DBCollection collection = getCollection();
			List<DBObject> dbObjects = convertModel(models);
			for (DBObject dbObject : dbObjects) {
				collection.save(dbObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection();
		}
	}
	
	public void update(M model) {
		// update code here
	}
	
	public void delete(M model) {
		// delete code here
	}
	
	/**
	 * Get distinct values for a collection field
	 * @param field
	 * @param collectionName
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public List distinctValues(String field) {
		List values = new ArrayList<String>();
		
		try {
			DBCollection collection = getCollection();
			values = collection.distinct(field);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection();
		}
		
		return values;
	}
	
	/**
	 * 
	 * @param model
	 * @return
	 */
	protected BasicDBObject convertModel(M model) {
		return (BasicDBObject) JSON.parse(gson.toJson(model));
	}
	
	/**
	 * 
	 * @param models
	 * @return
	 */
	protected List<DBObject> convertModel(List<M> models) {
		List<DBObject> objects = new ArrayList<DBObject>();
		for (M model : models) {
			objects.add(convertModel(model));
		}
		return objects;
	}
	
	/**
	 * 
	 * @param object
	 * @param clazz
	 * @return
	 */
	protected M convertJson(DBObject object, Class<M> clazz) {
		return gson.fromJson(object.toString(), clazz);
	}

	/*
	 * Abstract methods
	 */
	public abstract List<M> find(DBObject query, DBObject sort);
//	public abstract void update(M model);
//	public abstract void delete(M model);
	
}