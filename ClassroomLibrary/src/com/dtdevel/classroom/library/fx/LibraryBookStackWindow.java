package com.dtdevel.classroom.library.fx;

import java.io.InputStream;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

import com.dtdevel.classroom.library.models.LibraryBook;
import com.dtdevel.classroom.library.util.ApplicationSetup;

public class LibraryBookStackWindow extends LibraryStackWindow<LibraryBook> {

	public LibraryBookStackWindow(LibraryBook libraryBook) {
		super(libraryBook);
	}

	@Override
	protected Node setupModelContent() {
		// Create window image
		InputStream input = ApplicationSetup.createImageStream(model.getLargestImage());
		Image image = new Image(input);
		ImageView imageView = new ImageView(image);
    	imageView.setSmooth(true);
    	imageView.setCache(true);
		imageView.setFitWidth(300);
		imageView.setPreserveRatio(true);
		
		// Book details box
		VBox detailsBox = new VBox(5);
		detailsBox.setPadding(new Insets(15,5,15,5));
		Label titleLabel = new Label(model.getTitle());
		titleLabel.setWrapText(true);
		Label authorLabel = new Label(model.getAuthor());
		authorLabel.setWrapText(true);
		Label categoryLabel = new Label(model.getCategory());
		categoryLabel.setWrapText(true);
		Label proseLabel = new Label(model.getProse());
		proseLabel.setWrapText(true);
		Label readingLevelLabel = new Label(String.format("Reading Level: %.1f", model.getLevel()));
		readingLevelLabel.setWrapText(true);
		Label numCopiesLabel = new Label(String.format("%d copies", model.getNumCopies()));
		numCopiesLabel.setWrapText(true);
		detailsBox.getChildren().addAll(titleLabel, authorLabel, categoryLabel, proseLabel, readingLevelLabel, numCopiesLabel);

		GridPane bookGrid = new GridPane();
		bookGrid.add(imageView, 0, 0);
		bookGrid.add(detailsBox, 1, 0);
		bookGrid.setMaxWidth(Region.USE_PREF_SIZE);
		bookGrid.setMaxHeight(Region.USE_PREF_SIZE);
		bookGrid.setStyle("-fx-background-color: white; -fx-border-color: black; -fx-border-width: 5px;");

		return bookGrid;
	}
}
