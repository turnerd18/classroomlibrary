package com.dtdevel.classroom.library.fx;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import com.dtdevel.classroom.library.models.LibraryBook;
import com.dtdevel.classroom.library.ui.ClassroomLibrary;
import com.dtdevel.classroom.library.util.ApplicationSetup;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class LibraryBookTile extends BorderPane {

	private static Boolean isLoadingWindow = false;
	
	private LibraryBook libraryBook;
	
	public final static double thumbnailWidth = 128;
	
	public LibraryBook getLibraryBook() {
		return libraryBook;
	}
	
	public static void DoneLoadingWindow() {
		isLoadingWindow = false;
	}
	
	public LibraryBookTile(LibraryBook libraryBook) {
		super();

		// Title label
    	Label titleLabel = new Label(libraryBook.getTitle());
    	titleLabel.setWrapText(true);
    	titleLabel.setAlignment(Pos.TOP_LEFT);
    	titleLabel.setFont(Font.font(java.awt.Font.SERIF, FontWeight.BOLD, 14));
    	titleLabel.setTextFill(Color.ANTIQUEWHITE);
    	titleLabel.setPrefHeight(titleLabel.getFont().getSize() * 4);
		super.setBottom(titleLabel);
		
		// Book image
    	ImageView imageView = new ImageView();
    	try {
    		File imageFile = null;
			imageFile = new File(ApplicationSetup.getThumbnailPath(libraryBook));
			if (imageFile.exists()) {
	    		InputStream is = new FileInputStream(imageFile);
	    		Image image = new Image(is);
				imageView = new ImageView(image);
			} else {
				ApplicationSetup.createImageFile(libraryBook);
    			imageFile = new File(ApplicationSetup.getThumbnailPath(libraryBook));
    			if (imageFile.exists()) {
    	    		InputStream is = new FileInputStream(imageFile);
    	    		Image image = new Image(is);
    				imageView = new ImageView(image);
    			}
			}
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	imageView.setSmooth(true);
    	imageView.setCache(true);
    	imageView.setFitWidth(thumbnailWidth);
    	imageView.setPreserveRatio(true);
    	
    	// Reading Level text
    	Label levelLabel = new Label(String.format("%.1f", libraryBook.getLevel()));
    	levelLabel.setAlignment(Pos.TOP_RIGHT);
    	levelLabel.setFont(Font.font(java.awt.Font.SANS_SERIF, FontWeight.EXTRA_BOLD, 18));
    	levelLabel.setTextFill(Color.DODGERBLUE.darker());
    	
    	// Label pane with circle background
    	final VBox levelLabelPane = new VBox();
    	levelLabelPane.getChildren().add(levelLabel);
    	levelLabelPane.getStyleClass().add("readingLevelCircle");
    	levelLabelPane.setMaxWidth(USE_PREF_SIZE);
    	levelLabelPane.setMaxHeight(USE_PREF_SIZE);
    	
    	// Book image stack
    	StackPane bookStack = new StackPane();
    	bookStack.getChildren().addAll(imageView, levelLabelPane);
    	StackPane.setAlignment(levelLabelPane, Pos.BOTTOM_RIGHT);
    	
    	super.setCenter(bookStack);
    	
		super.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				if (isLoadingWindow) {
					return;
				}
				
				isLoadingWindow = true;
				
				ClassroomLibrary.getScene().setCursor(Cursor.WAIT);				
				final LibraryBook libraryBook = ((LibraryBookTile)event.getSource()).getLibraryBook();
				LibraryBookStackWindow bookWindow = new LibraryBookStackWindow(libraryBook);
				ClassroomLibrary.getMainStack().getChildren().add(bookWindow);
				bookWindow.setupContent();
			}
		});
		super.setOnMouseEntered(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				if (!isLoadingWindow) {
					ClassroomLibrary.getScene().setCursor(Cursor.HAND);
				}
			}
		});
		super.setOnMouseExited(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				if (!isLoadingWindow) {
					ClassroomLibrary.getScene().setCursor(Cursor.DEFAULT);
				}
			}
		});
		
		this.libraryBook = libraryBook;
	}
}
