package com.dtdevel.classroom.library.fx;

import com.dtdevel.classroom.library.ui.ClassroomLibrary;

import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;

public abstract class LibraryStackWindow<T> extends BorderPane {
		
	protected final T model;
	
	protected Node content;
	
	private static Boolean windowLoadCanceled = false;
	
	public static Boolean windowIsShowing = false;
	
	protected LibraryStackWindow(T model) {
		super();
		super.setStyle("-fx-background-color: rgb(0,0,0,0.5);");
		super.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				if (!windowIsShowing) {
					windowLoadCanceled = true;
				}
				StackPane stack = ClassroomLibrary.getMainStack();
				Node booksPane = stack.getChildren().get(0);
				stack.getChildren().clear();
				stack.getChildren().add(booksPane);
				LibraryBookTile.DoneLoadingWindow();
				windowIsShowing = false;
			}
		});
		
		content = new Label("");
		super.setCenter(content);
		
		this.model = model;
	}
	
	public void setupContent() {
		final Task<Node> task = new Task<Node>() {
			@Override
			protected Node call() throws Exception {
				return setupModelContent();
			}
		};
		
		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			@Override
			public void handle(WorkerStateEvent event) {
				if (windowLoadCanceled) {
					windowLoadCanceled = false;
					return;
				}
				
				BorderPane root = (BorderPane) content.getParent();
				
				content = task.getValue();
				content = setupModelContent();
				
				if (content == null) {
					return;
				}
				
				content.setOnMouseClicked(new EventHandler<Event>() {
					@Override
					public void handle(Event event) {
						event.consume();
						return;
					}
				});
				
				root.setCenter(content);
				ClassroomLibrary.getScene().setCursor(Cursor.DEFAULT);
				LibraryBookTile.DoneLoadingWindow();
				windowIsShowing = true;
			}
		});
		
		new Thread(task).start();
	}
	
	protected abstract Node setupModelContent(); 
}
