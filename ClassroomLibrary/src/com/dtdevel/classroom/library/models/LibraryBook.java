package com.dtdevel.classroom.library.models;

import com.google.api.services.books.model.Volume.VolumeInfo.ImageLinks;

public class LibraryBook {
	
	public static int THUMBNAIL_SIZE = 128;
	
	private int ID;
	private String title;
	private String author;
	private float level;
	private String category;
	private int numCopies;
	private String prose;
	private String googleVolumeId;
	private ImageLinks imageLinks;
	
	/*
	 * Getters & Setters
	 */
	public int getID() {
		return ID;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public float getLevel() {
		return level;
	}
	public void setLevel(float level) {
		this.level = level;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public int getNumCopies() {
		return numCopies;
	}
	public void setNumCopies(int numCopies) {
		this.numCopies = numCopies;
	}
	public String getProse() {
		return prose;
	}
	public void setProse(String prose) {
		this.prose = prose;
	}
	public String getGoogleVolumeId() {
		return googleVolumeId;
	}
	public void setGoogleVolumeId(String googleVolumeId) {
		this.googleVolumeId = googleVolumeId;
	}
	public ImageLinks getImageLinks() {
		return imageLinks;
	}
	public void setImageLinks(ImageLinks imageLinks) {
		this.imageLinks = imageLinks;
	}
	
	/*
	 * Constructors
	 */
	public LibraryBook(int ID) {
		this.ID = ID;
	}
	
//	public LibraryBook() {
//		
//	}
	/*
	 * Methods
	 */
	public String toString() {
		int numLinks = imageLinks == null ? 0 : imageLinks.size();
		return String.format("%d|%s|%s|%.1f|%s|%d|%s|%d images", ID, title, author, level, category, numCopies, prose, numLinks);
	}
	
	public String getLargestImage() {
		if (imageLinks == null)
			return null;
//		else if (imageLinks.getExtraLarge() != null)
//			return imageLinks.getExtraLarge();
		else if (imageLinks.getLarge() != null)
			return imageLinks.getLarge();
		else if (imageLinks.getMedium() != null)
			return imageLinks.getMedium();
		else if (imageLinks.getSmall() != null)
			return imageLinks.getSmall();
		else if (imageLinks.getThumbnail() != null)
			return imageLinks.getThumbnail();
		else if (imageLinks.getSmallThumbnail() != null)
			return imageLinks.getSmallThumbnail();
		else
			return null;
	}
}
