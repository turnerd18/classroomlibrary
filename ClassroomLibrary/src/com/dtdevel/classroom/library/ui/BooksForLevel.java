package com.dtdevel.classroom.library.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.dtdevel.classroom.library.dataaccess.LibraryBookDataAccess;
import com.dtdevel.classroom.library.fx.LibraryBookTile;
import com.dtdevel.classroom.library.models.LibraryBook;
import com.dtdevel.classroom.library.util.FontHelper;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import javafx.animation.FadeTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.TilePane;
import javafx.util.Duration;

public class BooksForLevel {
	
	private static List<BorderPane> bookPanes;
	
	public static void BuildMainContent(double readingLevel) {
		// Reading level integers
		@SuppressWarnings("unchecked")
		List<Double> levelDoubles = LibraryBookDataAccess.getInstance().distinctValues("level");
		List<Integer> levels = new ArrayList<Integer>();
		Collections.sort(levelDoubles);
		for (Double levelDouble : levelDoubles) {
			int level = (int)levelDouble.doubleValue();
			if (!levels.contains(level)) {
				levels.add(level);
			}
		}
		
		// ComboBox for switching levels
		ComboBox<Integer> levelsBox = new ComboBox<Integer>();
		levelsBox.setValue((int)readingLevel);
		levelsBox.getItems().addAll(levels);
		levelsBox.valueProperty().addListener(new ChangeListener<Integer>() {
			@Override
			public void changed(ObservableValue<? extends Integer> observable,
					Integer oldValue, Integer newValue) {
				BuildMainContent(newValue);
			}
		});
		
		// Get books for level
		BasicDBList andQuery = new BasicDBList();
		andQuery.add(new BasicDBObject("level", new BasicDBObject("$gte", (int)readingLevel)));
		andQuery.add(new BasicDBObject("level", new BasicDBObject("$lt", (int)readingLevel + 1)));
		DBObject query = new BasicDBObject("$and", andQuery);
		BasicDBObject sort = new BasicDBObject("level", 1);
		List<LibraryBook> books = LibraryBookDataAccess.getInstance().find(query, sort);

		final double thumbnailWidth = 128;
        
		// BorderPane for each book
        bookPanes = new ArrayList<BorderPane>();
        for (LibraryBook libraryBook : books) {
        	LibraryBookTile libraryBookTile = new LibraryBookTile(libraryBook);
        	FadeTransition ft = new FadeTransition(Duration.seconds(1), libraryBookTile);
        	ft.setFromValue(0);
        	ft.setToValue(1);
        	ft.play();
        	
			bookPanes.add(libraryBookTile);
		}

        TilePane booksPane = new TilePane(10, 10); 
        booksPane.getChildren().addAll(bookPanes);
        booksPane.setPrefTileWidth(thumbnailWidth);
        
        Label titleLabel = new Label("Ms. Hopkins' Library");
        titleLabel.setFont(FontHelper.getOSTTFFont("waltographUI.ttf", 48));
        titleLabel.setPrefHeight(60);
        
		HBox navBox = new HBox(10);
		navBox.getChildren().addAll(new Label("Reading Level:"), levelsBox);
		navBox.setAlignment(Pos.CENTER_RIGHT);
        
        BorderPane topPane = new BorderPane();
        topPane.setCenter(titleLabel);
        topPane.setRight(navBox);
		topPane.setPadding(new Insets(20, 20, 20, 20));
		topPane.setStyle("-fx-background-color: beige;");

		ClassroomLibrary.getMainContent().setTop(topPane);
		ClassroomLibrary.getMainContent().setCenter(booksPane);
//        ClassroomLibrary.getMainContent().setStyle("-fx-background-color: honeydew;");
//        ClassroomLibrary.getMainContent().getStyleClass().add("booksPane");
	}
}
