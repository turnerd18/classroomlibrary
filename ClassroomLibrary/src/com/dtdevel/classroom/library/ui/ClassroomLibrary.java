package com.dtdevel.classroom.library.ui;
	
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import com.dtdevel.classroom.library.util.ApplicationSetup;

public class ClassroomLibrary extends Application {
	
	private static final String SETUP_PREF = "isSetup";
	
	private static BorderPane mainContent;
	private static StackPane mainStack;
	private static BorderPane blackOverlay;
	private static List<Pane> fitToScene;
	private static Scene scene;
	private static ScrollPane scrollPane;
	
	public static BorderPane getMainContent() {
		return mainContent;
	}
	
	public static StackPane getMainStack() {
		return mainStack;
	}
	
	public static BorderPane getBlackOverlay() {
		return blackOverlay;
	}
	
	public static List<Pane> getFitToScene() {
		return fitToScene;
	}
	
	public static Scene getScene() {
		return scene;
	}
	
	@Override
	public void start(Stage stage) {

		// Check if setup from preferences
		Preferences prefs = Preferences.userNodeForPackage(this.getClass());
		if (!prefs.getBoolean(SETUP_PREF, false)) {
			System.out.println("checked prefs");
			try {
				ApplicationSetup.Run();
				prefs.putBoolean(SETUP_PREF, true);
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}
		}
		
		mainContent = new BorderPane();
		scrollPane = new ScrollPane();
		scrollPane.setHbarPolicy(ScrollBarPolicy.NEVER);
		scrollPane.setContent(mainContent);
		scrollPane.getStyleClass().add("booksPane");
				
		blackOverlay = new BorderPane();
		blackOverlay.setStyle("-fx-background-color: black;");
		blackOverlay.setOpacity(0);
		blackOverlay.setDisable(true);
		
		mainStack = new StackPane();
		mainStack.getChildren().addAll(scrollPane);
		
		fitToScene = new ArrayList<Pane>();
		fitToScene.add(mainContent);
		
		BooksForLevel.BuildMainContent(3.0);
		
		scene = new Scene(mainStack,1000,400);
		scene.getStylesheets().add(getClass().getResource("classroom_library.css").toExternalForm());
		scene.widthProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable,
					Number oldValue, Number newValue) {
				updateSceneWidth(newValue.doubleValue());
			}
		});

		stage.setScene(scene);
		stage.addEventHandler(WindowEvent.WINDOW_SHOWN, new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				Scene scene = ((Stage)event.getSource()).getScene();
				updateSceneWidth(scene.getWidth());
			}
		});
		stage.show();
	}
	
	private static void updateSceneWidth(double width) {
		for (Pane pane : fitToScene) {
			pane.setPrefWidth(width);
		}
		scrollPane.setPrefWidth(width);
	}
	
	public static void main(String[] args) {
		launch(args);	
	}
}
