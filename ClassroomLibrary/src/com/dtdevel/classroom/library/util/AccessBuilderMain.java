package com.dtdevel.classroom.library.util;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

import com.dtdevel.classroom.library.dataaccess.LibraryBookDataAccess;
import com.dtdevel.classroom.library.models.LibraryBook;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.books.Books;
import com.google.api.services.books.BooksRequestInitializer;
import com.google.api.services.books.model.Volume;
import com.google.api.services.books.model.Volumes;
import com.healthmarketscience.jackcess.DatabaseBuilder;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.mongodb.BasicDBObject;

public class AccessBuilderMain {

	private final static String APPLICATION_NAME = "ClassroomLibrary";
	private final static String GOOGLE_BOOKS_API_KEY = "AIzaSyAv5mLq7gEG6p_ZkLY4fAVcqLsbp9ajP10";
	
	public static void main(String[] args) {
		BuildBookCollection();
		CheckBookCollection();
	}
	
	private static void BuildBookCollection() {	
		try {
			LibraryBookDataAccess.getInstance().dropCollection();
			
			// Read from Access database
			Table dbTable = DatabaseBuilder.open(new File("Library Books.accdb")).getTable("Copy of Library Books");
			List<LibraryBook> books = new ArrayList<LibraryBook>();
			
			// Setup Google Books access
			final Books gBooks = new Books.Builder(GoogleNetHttpTransport.newTrustedTransport(), JacksonFactory.getDefaultInstance(), null)
		        .setApplicationName(APPLICATION_NAME)
		        .setGoogleClientRequestInitializer(new BooksRequestInitializer(GOOGLE_BOOKS_API_KEY))
		        .build();
			
			for (Row row : dbTable) {
				int ID = 0;
				try {
					ID = Integer.parseInt(row.get("ID").toString());
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (ID == 0)
					continue;
				
				// create new library book
				LibraryBook newBook = new LibraryBook(ID);
				newBook.setTitle(row.get("Title").toString());
				newBook.setAuthor(row.get("Author").toString());
				float level = 0;
				try {
					level = Float.parseFloat(row.get("Level").toString());
				} catch (Exception e) {
					e.printStackTrace();
				}
				newBook.setLevel(level);
				newBook.setCategory(row.get("Category").toString());
				newBook.setTitle(row.get("Title").toString());
				int numCopies = 1;
				try {
					numCopies = Integer.parseInt(row.get("NumCopies").toString());
				} catch (Exception e) {
					e.printStackTrace();
				}
				newBook.setNumCopies(numCopies);
				newBook.setProse(row.get("(Non)Fiction").toString());
				
				// Get book image from Google Books
				String authorSplit[] = newBook.getAuthor().split(" ");
				String authorLast = authorSplit[authorSplit.length -1];
				String query = String.format("intitle:%s+inauthor:%s", newBook.getTitle(), authorLast);
				
				com.google.api.services.books.Books.Volumes.List volumesList = gBooks.volumes().list(query).setMaxResults((long) 1).setFields("items(id,volumeInfo/imageLinks)");
				Volumes volumes = volumesList.execute();
				if (volumes != null && volumes.getItems() != null && !volumes.getItems().isEmpty()) {
					Volume volume = gBooks.volumes().get(volumes.getItems().get(0).getId()).setFields("volumeInfo/imageLinks").execute();
					if (volume != null && volume.getVolumeInfo() != null)
						newBook.setImageLinks(volume.getVolumeInfo().getImageLinks());
				}

				LibraryBookDataAccess.getInstance().insert(newBook);			
				books.add(newBook);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		}
	}
	
	private static void CheckBookCollection() {
		BasicDBObject query = null;
		List<LibraryBook> books = LibraryBookDataAccess.getInstance().find(query, null);
		List<LibraryBook> noImages = new ArrayList<LibraryBook>();
		for (LibraryBook book : books) {
			if (book.getImageLinks() == null || book.getImageLinks().size() == 0)
				noImages.add(book);
			System.out.println(book.toString());
		}
		
		System.out.print(String.format("\n\nMISSING IMAGES\n%d books without images: ", noImages.size()));
		for (LibraryBook noImage : noImages) {
			System.out.print(String.format("%d ", noImage.getID()));
		}
	}
}
