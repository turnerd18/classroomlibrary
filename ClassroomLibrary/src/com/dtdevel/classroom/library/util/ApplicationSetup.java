package com.dtdevel.classroom.library.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import com.dtdevel.classroom.library.dataaccess.LibraryBookDataAccess;
import com.dtdevel.classroom.library.models.LibraryBook;

public class ApplicationSetup {
	
	public final static String THUMBNAILS_PATH = "./Resources/thumbnails";
	
	public static void Run() throws Exception {
		File thumbnailsFolder = new File(THUMBNAILS_PATH);
		
		// create directory if it does exist. error otherwise
		if (!thumbnailsFolder.exists()) {
			try {
			thumbnailsFolder.mkdir();
			} catch (SecurityException e) {
				throw new Exception("Could not create thumbnails folder.");
			}
		} else if (!thumbnailsFolder.isDirectory()) {
			throw new Exception("Thumbnails exist but are not a folder.");
		}
		
		// delete contents
		deleteFolderContents(thumbnailsFolder);
		
		// create images for books in collection
		List<LibraryBook> libraryBooks = LibraryBookDataAccess.getInstance().find(null, null);
		for (LibraryBook libraryBook : libraryBooks) {
			createImageFile(libraryBook);
		}
	}
	
	private static void deleteFolderContents(File folder) {
		File[] files = folder.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				deleteFolderContents(file);
			} else {
				file.delete();
			}
		}
	}
	
	/**
	 * Create a local image file in RESOURCES for the book 
	 * @param libraryBook
	 */
	public static void createImageFile(LibraryBook libraryBook) {
		InputStream input = null;
		OutputStream output = null;
		try {
			if (libraryBook.getImageLinks() == null) {
				input = createImageStream(null);
			} else {
				input = createImageStream(libraryBook.getImageLinks().getThumbnail());
			}
			output = new FileOutputStream(getThumbnailPath(libraryBook));
			byte[] buf = new byte[1024];
			int bytesRead;
			while ((bytesRead = input.read(buf)) > 0) {
				output.write(buf, 0, bytesRead);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (input != null) {
					input.close();
				}
				if (output != null) {
					output.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static InputStream createImageStream(String imageUrl) {
		InputStream input = null;
		try {
		if (imageUrl == null || imageUrl.isEmpty()) {
			input = new FileInputStream("./Resources/no_image.jpg");
		} else {
			URL url = new URL(imageUrl);
	    	URLConnection urlConn = url.openConnection();
	        urlConn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.21; Mac_PowerPC)" ); 
	        urlConn.connect();
			input = urlConn.getInputStream();
		}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return input;
	}
	
	/**
	 * Return the 
	 * @param libraryBook
	 * @return
	 */
	public static String getThumbnailPath(LibraryBook libraryBook) {
		return String.format("%s/%d.png", THUMBNAILS_PATH, libraryBook.getID());
	}
}
