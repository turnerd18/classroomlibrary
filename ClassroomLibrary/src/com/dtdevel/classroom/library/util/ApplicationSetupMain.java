package com.dtdevel.classroom.library.util;

public class ApplicationSetupMain {

	public static void main(String[] args) {
		try {
			ApplicationSetup.Run();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
