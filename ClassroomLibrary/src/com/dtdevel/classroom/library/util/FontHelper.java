package com.dtdevel.classroom.library.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import javafx.scene.text.Font;

public class FontHelper {

	public static Font getOSTTFFont(String fontName, double size) {
		String os = System.getProperty("os.name").toLowerCase();
		String osFontFolder = os.contains("mac") ? "__MACOSX/" : "";
		File ttfFile = new File(String.format("./Resources/fonts/%s%s", osFontFolder, fontName));
		Font font;
		try {
			InputStream is = new FileInputStream(ttfFile);
			font = Font.loadFont(is, size);
		} catch (FileNotFoundException e) {
			font = Font.getDefault();
			e.printStackTrace();
		}
		return font;
	}
}
